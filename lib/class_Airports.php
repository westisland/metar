<?php


class Airports
{
	public $airports;
	
	function __construct()
	{
		$data = json_decode(file_get_contents(__DIR__ . "/../cache/cacheAroLfv_Metar.ser"));
		foreach ($data as $key => $value)
		{
			$this->airports[$key] = array();
			$this->airports[$key]['CODE'] = $key; //array();
		}
	}
	
	function loadCachedData()
	{
		$data = json_decode(file_get_contents(__DIR__ . "/../cache/cacheAroLfv_Metar.ser"));
		foreach ($data as $key => $value)
			{
				$this->airports[$key]['METAR'] = $value->METAR;
			}
		
		$data = json_decode(file_get_contents(__DIR__ . "/../cache/cacheAroLfv_Taf.ser"));
		foreach ($data as $key => $value)
			{
				$this->airports[$key]['TAF'] = $value->TAF;
			}
		
		
		$data = json_decode(file_get_contents(__DIR__ . "/../cache/cacheAroLfv_AirPortsFromWikipedia.ser"));
		foreach ($data as $key => $value)
			{
				if (isset($this->airports[$key]))
					{
						$this->airports[$key]['Name'] = $value->Name;
						$this->airports[$key]['WikipediaUrl'] = $value->WikipediaUrl;
					}
			}
	}
	
	public function getAirportNameFromAirportCode($airportCode)
	{
		
		
	}
	
	public function searchAirportsViaAirportCodes($search)
	{
		
		
	}
	
	public function searchAirportsViaAirportNames($search)
	{
		
		
	}
	
	public function getAirPortNamesFromWikiPedia()
	{
		//<li><b>ENSD</b> (SDN) – <a href="/wiki/Sandane_Airport,_Anda" title="Sandane Airport, Anda">Sandane Airport, Anda</a>
		$data = file_get_contents("https://en.wikipedia.org/wiki/List_of_airports_by_ICAO_code:_E#ES_.E2.80.93_Sweden");
		preg_match_all("/<li><b>(\w\w\w\w)<\/b>(.*?)<\/a>/msi",$data,$match);
		$lists[1] = $match[1];
		$lists[2] = $match[2];
		
		foreach($match[1] as $key => $value)
		{
			$airportName = $match[2][$key];
			preg_match("/title=.*?>(.*)/msi", $airportName,$aMatch);
			preg_match("/href=\"(.*?)\"/msi", $airportName,$urlMatch);
			$airPortsFromWiki[$value]['Name'] = $aMatch[1]; //$match[2][$key];
			$airPortsFromWiki[$value]['WikipediaUrl'] = "http://wikipedia.com" . $urlMatch[1]; //$match[2][$key];
		}
		//print_r($airPortsFromWiki);
		
		$jsonAirPortsFromWiki = json_encode($airPortsFromWiki);
		file_put_contents(__DIR__ . "/../cache/cacheAroLfv_AirPortsFromWikipedia.ser",$jsonAirPortsFromWiki);
		return $jsonAirPortsFromWiki;
		
	}
}
