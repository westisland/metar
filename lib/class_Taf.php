<?php


class Taf
{
	
	
	public function readCachedMetar()
	{
		//METAR
		return json_decode(file_get_contents(__DIR__ . "/../cache/cacheAroLfv_Metar.ser"));
	}
	
	
	public function readCachedTaf()
	{
		//TAF
		return json_decode(file_get_contents(__DIR__ . "/../cache/cacheAroLfv_Taf.ser"));	
	}
	
	
}
