<?php


/*
	
	This class is for reading data from https://aro.lfv.se/
	
*/

class AroLfv
{
	public $airportData;
	
	function __construct ($airportDataPointer)
	{
		$this->airportData = $airportDataPointer;
		
	}
	
	
	public function readAndCacheMetar()
	{
		//METAR
		$data = $this->readAndReturnMetar();
		
		$jsonEncodedMetar = json_encode($data);
		file_put_contents(__DIR__ . "/../cache/cacheAroLfv_Metar.ser",$jsonEncodedMetar);
		return $jsonEncodedMetar;
		
	}
	
	
	public function readAndCacheTaf()
	{
		
		//TAF
		$data = $this->readAndReturnTaf();
		
		$jsonEncodedTaf = json_encode($data);
		file_put_contents(__DIR__ . "/../cache/cacheAroLfv_Taf.ser",$jsonEncodedTaf);
		return $jsonEncodedTaf;
				
		
	}
	
	
	
	
	//Read METAR From aro.lfv.se
	public function readAndReturnMetar()
	{
		
		$debug=0;
		
		$data = file_get_contents("https://aro.lfv.se/Links/Link/ViewLink?TorLinkId=300&type=MET");
		if($debug) print $data;
		preg_match("/<section class=.panel.>.*?<h1 class=.tor-link-header.> Sverige(.*?)<h1/msi",$data,$match);
		$data = $match[1];
		
		preg_match_all("/<span class=.tor-link-text-row-item item-header.>(\w\w\w\w)<\/span>.*?<span class=.tor-link-text-row-item item-text.>(.*?)<\/span/msi",$data,$match);
		if($debug) print_r($match);
		
		
		$airports = $match[1];
		$metars = $match[2];
		
		if (count($airports) == count($metars))
		{
			
			
		}
		else
		{
				//Close down the site.. since the parsed data can not be trusted..
			
		}
		
		foreach($airports as $key => $value)
			{
				$metar[$value]['CODE'] =  $value; 
				$metar[$value]['METAR'] =  $metars[$key];
			}
		if($debug) print_r($metar);
		if($debug) die;
		
		
		return $metar;
	}
	
	//Read TAF From aro.lfv.se
	public function readAndReturnTaf()
	{
		
		
		
		$data = file_get_contents("https://aro.lfv.se/Links/Link/ViewLink?TorLinkId=304&type=MET");
		//print $data;
		preg_match("/<section class=.panel.>.*?<h1 class=.tor-link-header.>Sverige(.*?)<h1/msi",$data,$match);
		$data = $match[1];
		
		preg_match_all("/<span class=.tor-link-text-row-item item-header.>(\w\w\w\w)<\/span>.*?<span class=.tor-link-text-row-item item-text.>(.*?)<\/span/msi",$data,$match);
		//print_r($match);
		
		
		$airports = $match[1];
		$tafs = $match[2];
		
		if (count($airports) == count($tafs))
		{
			
			
		}
		else
		{
				//Close down the site.. since the parsed data can not be trusted..
			
		}
		
		foreach($airports as $key => $value)
			{
				$taf[$value]['CODE'] =  $value; 
				$taf[$value]['TAF'] =  $tafs[$key];
			}
		//print_r($taf);
		
		
		return $taf;
		
	}
	
}
