<?php


include(__DIR__ . "/lib/classes_include.php");


$airports = new Airports();


$airports->loadCachedData();
//print_r($airports->airports);

include("tophtml.php");


print '<table  class="table table-hover"> <tbody>';
print "\n";

foreach($airports->airports as $airport)
{
	//print_r($airport);
	print '<tr>';
	print '<td>';
	print '<h3>' . $airport['CODE']  . " - " . $airport['Name'] . '</h3>';
	print '<br>';
	print '<b>METAR</b>: '; 
	print $airport['METAR'];
	print '<br>';
	print '<b>TAF</b>: '; 
	if(isset($airport['TAF'])) print $airport['TAF'];
	
	print '<br>';
	print '</td>';
	print '</tr>';
	print "\n";
	
}


print '
</tbody>
</table>
';



include("endhtml.php");

/*
$data = $aroLfv->readAndCacheMetar();

$data = $aroLfv->readAndReturnMetar();

print_r($data);
*/

